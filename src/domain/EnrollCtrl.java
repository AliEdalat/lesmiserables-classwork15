package domain;

import java.util.List;
import domain.exceptions.EnrollmentRulesViolationException;

public class EnrollCtrl {
	public void enroll(Student student, List<Offering> offeringList) throws EnrollmentRulesViolationException {

		checkPreRequisites(student, offeringList);

		CheckNotPassedTheCourses(student, offeringList);

		CheckNoConfilictCourse(offeringList);

		CheckNumberOfCredit(student, offeringList);

		TakeOfferings(student, offeringList);
	}

	private void TakeOfferings(Student student, List<Offering> offeringList) {
		for (Offering o : offeringList)
			student.takeOffering(o);
	}

	private void CheckNumberOfCredit(Student student, List<Offering> offeringList) throws EnrollmentRulesViolationException {
		int unitsRequested = 0;
		for (Offering offering : offeringList)
			unitsRequested += offering.getUnits();

		double gpa = student.getGpa();

		if (!IsNumberOfUnitsMatch(unitsRequested, gpa))
			throw new EnrollmentRulesViolationException(String.format("Number of units (%d) requested does not match GPA of %f", unitsRequested, gpa));
	}

	private boolean IsNumberOfUnitsMatch(int unitsRequested, double gpa) {
		return !((gpa < 12 && unitsRequested > 14) ||
				(gpa < 16 && unitsRequested > 16) ||
				(unitsRequested > 20));
	}

	private void CheckNoConfilictCourse(List<Offering> offeringList) throws EnrollmentRulesViolationException {
		for (Offering offering : offeringList) {
			for (Offering offering1 : offeringList) {
				if (offering == offering1)
					continue;
				if (offering.getExamTime().equals(offering1.getExamTime()))
					throw new EnrollmentRulesViolationException(String.format("Two offerings %s and %s have the same exam time", offering, offering1));
				if (offering.getCourse().equals(offering1.getCourse()))
					throw new EnrollmentRulesViolationException(String.format("%s is requested to be taken twice", offering.getCourse().getName()));
			}
		}
	}

	private void CheckNotPassedTheCourses(Student student, List<Offering> offeringList) throws EnrollmentRulesViolationException {
		for (Offering offering : offeringList) {
			if(student.isCoursePassed(offering.getCourse()))
				throw new EnrollmentRulesViolationException(String.format("The student has already passed %s", offering.getCourse().getName()));
		}
	}

	private void checkPreRequisites(Student student, List<Offering> offeringList) throws EnrollmentRulesViolationException {
		for (Offering offering : offeringList) {
			List<Course> prereqs = offering.getCourse().getPrerequisites();
			for (Course pre : prereqs) {
				if (!student.isCoursePassed(pre))
					throw new EnrollmentRulesViolationException(String.format("The student has not passed %s as a prerequisite of %s", pre.getName(), offering.getCourse().getName()));
			}
		}
	}
}
